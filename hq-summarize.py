import argparse
import datetime

import pandas as pd
from datetime import timedelta
import sys
import os


def print_formatted_table(df):
    headers = list(df.columns)
    max_lengths = [max([len(str(s)) for s in df[col]] + [len(col)]) for col in headers]

    header_str = "| "
    for i, header in enumerate(headers):
        if header == "Arbeitszeit (hh:mm)":
            header_str += header.rjust(max_lengths[i]) + " | "
        else:
            header_str += header.ljust(max_lengths[i]) + " | "
    print(header_str)
    print("-" * (sum(max_lengths) + len(max_lengths) * 3 + 1))

    for _, row in df.iterrows():
        row_str = "| "
        for i, item in enumerate(row):
            if headers[i] == "Arbeitszeit (hh:mm)":
                row_str += str(item).rjust(max_lengths[i]) + " | "
            else:
                row_str += str(item).ljust(max_lengths[i]) + " | "
        print(row_str)


def main(file_path, start_date=None, end_date=None, filter_project_substring=None, group_descriptions=False):
    # Determine the file format and read data into a DataFrame
    _, file_extension = os.path.splitext(file_path)
    if file_extension == '.csv':
        df = pd.read_csv(file_path, delimiter=',', dtype={'Beschreibung': str})
    elif file_extension == '.xlsx':
        df = pd.read_excel(file_path, dtype={'Beschreibung': str})
    else:
        print("Unsupported file format. Please use .csv or .xlsx files.")
        sys.exit(1)

    # Convert 'Startdatum' to datetime format
    df['Startdatum'] = pd.to_datetime(df['Startdatum'], format='%d.%m.%y %H:%M')

    # Convert the date strings to datetime.date objects
    if start_date:
        start_date = datetime.datetime.strptime(start_date, '%Y-%m-%d').date()
    if end_date:
        end_date = datetime.datetime.strptime(end_date, '%Y-%m-%d').date()

    # Filter by date range if applicable
    if start_date and end_date:
        df = df[(df['Startdatum'].dt.date >= start_date) & (df['Startdatum'].dt.date <= end_date)]
    elif start_date:
        df = df[df['Startdatum'].dt.date >= start_date]
    elif end_date:
        df = df[df['Startdatum'].dt.date <= end_date]

    if filter_project_substring:
        df = df[df['Projekt'].str.contains(filter_project_substring, case=False, na=False)]

    # Convert the 'Arbeitszeit (hh:mm)' column to timedelta
    df['Arbeitszeit (hh:mm)'] = df['Arbeitszeit (hh:mm)'].apply(
        lambda x: timedelta(
            hours=int(x.split(':')[0]),
            minutes=int(x.split(':')[1].replace('h', ''))
        )
    )

    # Replace NaN with empty strings in 'Beschreibung' column
    df['Beschreibung'].fillna('- keine Beschreibung -', inplace=True)

    # Check which columns exist and should be used for hierarchical grouping
    group_by_columns = []
    if 'Projekt' in df.columns:
        group_by_columns.append('Projekt')
    if 'Benutzer' in df.columns:
        group_by_columns.append('Benutzer')
    group_by_columns.append('Aufgabe')


    # Define custom aggregation functions
    agg_funcs = {
        'Arbeitszeit (hh:mm)': 'sum'
    }

    if group_descriptions:
        group_by_columns.append('Beschreibung')
    else:
        agg_funcs['Beschreibung'] = lambda x: '; '.join(set(x))


    result = df.groupby(group_by_columns).agg(agg_funcs).reset_index()

    # Calculate overall sum
    overall_sum = result['Arbeitszeit (hh:mm)'].sum()

    # Sum by Project
    if 'Projekt' in df.columns:
        sum_per_project = df.groupby('Projekt')['Arbeitszeit (hh:mm)'].sum()

    # Convert the summed 'Arbeitszeit (hh:mm)' back to 'hh:mm' format
    result['Arbeitszeit (hh:mm)'] = result['Arbeitszeit (hh:mm)'].apply(
        lambda x: f"{int(x.total_seconds() // 3600)}:{int((x.total_seconds() % 3600) // 60):02}"
    )

    print_formatted_table(result)

    # Output Sum per Project
    if 'Projekt' in df.columns:
        print("\nSum per Project:")
        for proj, time in sum_per_project.items():
            print(f"{proj}: {int(time.total_seconds() // 3600)}:{int((time.total_seconds() % 3600) // 60):02}")

    print(f"\nOverall Sum: {int(overall_sum.total_seconds() // 3600)}:{int((overall_sum.total_seconds() % 3600) // 60):02}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process and summarize work time data from a CSV or Excel file.')

    # Argument for specifying the file path
    parser.add_argument('file', help='Path to the input CSV or Excel file')

    # Optional arguments for filtering by date
    parser.add_argument('--start-date', help='Filter tasks by start date (YYYY-MM-DD)', type=str, default=None)
    parser.add_argument('--end-date', help='Filter tasks by end date (YYYY-MM-DD)', type=str, default=None)

    # Optional argument for filtering by a project substring
    parser.add_argument('--project-substring', help='Filter by a substring of the project name', type=str, default=None)

    # Optional argument for grouping by descriptions
    parser.add_argument('--group-descriptions', help='Group by descriptions', action='store_true', default=False)

    args = parser.parse_args()

    main(args.file, start_date=args.start_date, end_date=args.end_date, filter_project_substring=args.project_substring, group_descriptions=args.group_descriptions)
